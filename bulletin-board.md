# CSE 109 - Bulletin Board

## :computer: Homework

- [ ] 02/17 - [Homework 2](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-2) - my_which (Updated Due Date)
- [x] 02/08 - [Homework 1](https://gitlab.com/lehigh-cse109/fall-2021/assignments/homework-1) - Learning Git
- [x] 02/03 - [Homework 0](https://gitlab.com/lehigh-cse109/fall-2021/course-info/-/blob/master/Homework-0.md) - Sign up for Gitlab

## :checkered_flag: Quizzes and Exams

- [ ] 02/16 - [Quiz 2](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-2)
- [x] 02/09 - [Quiz 1](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-1) - [Solutions](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-1/-/tree/solutions)

## :books: Readings

| Week                      | Readings           | 
| ------------------------- | ------------------ |
| Week 2 | <ul><li>[C/C++ Extension for VSC](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools) (Good resource for getting your VSC environment configured)</li><li>[Think C](https://github.com/tscheffl/ThinkC/blob/master/PDF/Think-C.pdf) - chapters 1-5. This should be mostly review as it is analogous to Java</li><li>[Putting a Program Together](http://crasseux.com/books/ctutorial/Putting-a-program-together.html#Putting%20a%20program%20together) - this is new to the C language compared to Java</li></ul>
| Week 1 | [The Missing Semester](https://missing.csail.mit.edu) - Chapters 1, 2, 5, 6 |

## :vhs: Lectures - [Playlist](https://youtube.com/playlist?list=PL4A2v89SXU3TS1fcYFmXi-tch8p0LucE7)

| Item                      | Date              | Content          | Links          |
| ------------------------- | ------------------ | ------------------ | --------------- |
|**Week 2**|
|Recitation 02 | 02/12 | Recitation 2 | [Video](https://drive.google.com/file/d/1IAuWqED9YbtYEbfB4bckji91xBIJZVKJ/view?usp=sharinghttps://drive.google.com/file/d/1IAuWqED9YbtYEbfB4bckji91xBIJZVKJ/view?usp=sharing) - [Code](https://gitlab.com/lehigh-cse-109/spring-2021/course-info/-/blob/master/lecture-code/recitation2/main.c)
|Lecture 04 | 02/10 | Anatomy of a C Program | [Video](https://youtu.be/Xh1LqbqKlo0) - [Code](https://gitlab.com/lehigh-cse-109/spring-2021/course-info/-/blob/master/project-template.zip)
|Lecture 03 | 02/08 | C and Unix | [Video](https://youtu.be/Bd21jOGT_GU)
|**Week 1**|
|Recitation 01 | 02/05 | Recitation 1 | [Video](https://drive.google.com/file/d/1eveEE1oJ9hIcRFamBZuaotOSMzCbhwGz/view?usp=sharing)
|Lecture 02 | 02/03 | Introduction to Git and C | [Video](https://youtu.be/Y0pARcUxQmo)
|Lecture 01 | 02/01 | Course Introduction | [Video](https://youtu.be/0eGRozEYNxA)

